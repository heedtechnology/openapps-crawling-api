package org.heed.openapps.crawling;
import java.io.StringWriter;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;

public class SeedImpl extends EntityImpl implements Seed {
	private static final long serialVersionUID = -6268988968223535266L;
	private byte[] content;
	
	
	public SeedImpl() {
		setQName(CrawlingModel.SEED);
	}
	public SeedImpl(Entity entity) {
		setEntity(entity);
	}
	public SeedImpl(String url, String name) {
		setQName(CrawlingModel.DOCUMENT);
		setUrl(url);
		setName(name);
		setTimestamp(System.currentTimeMillis());
	}
	public SeedImpl(JsonObject object) {
		setQName(CrawlingModel.SEED);
		if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
		if(object.containsKey("uid")) setUid(object.getString("uid"));
		if(object.containsKey("url")) setUrl(object.getString("url"));
		if(object.containsKey("status")) setStatus(object.getInt("status"));
	}
	
	public void setEntity(Entity entity) {
		setNode(entity.getNode());
		setProperties(entity.getProperties());
		setSourceAssociations(entity.getSourceAssociations());
		setTargetAssociations(entity.getTargetAssociations());
	}
	
	//Transient
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	
	public void setUrl(URL url) {
		try {
			setProtocol(url.getProtocol());
			setDomain(url.getHost());
			if(url.getPath() != null && url.getPath().length() > 0) setPath(url.getPath());
			if(url.getQuery() != null) setQuery(url.getQuery());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void setUrl(String urlStr) {
		try {
			URL url = new URL(urlStr);
			setProtocol(url.getProtocol());
			setDomain(url.getHost());
			if(url.getPath() != null && url.getPath().length() > 0) setPath(url.getPath());
			if(url.getQuery() != null) setQuery(url.getQuery());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public String getUrl() {
		String url = getProtocol() + "://" + getDomain();
		if(getPath() != null && getPath().length() > 0) url = url + getPath();
		if(getQuery() != null && getQuery().length() > 0) url = url + "?" + getQuery();
		return url;
	}
	public String getProtocol() {
		return getPropertyValue(SystemModel.PROTOCOL);
	}
	protected void setProtocol(String protocol) {
		try {
			addProperty(SystemModel.PROTOCOL, protocol);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+SystemModel.PROTOCOL+" -> "+protocol);
		}
	}
	public String getDomain() {
		return getPropertyValue(SystemModel.DOMAIN);
	}
	public void setDomain(String domain) {
		//if(domain != null) domain = domain.replace("google2.", "").replace("www.", "");
		try {
			addProperty(SystemModel.DOMAIN, domain);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+SystemModel.DOMAIN+" -> "+domain);
		}
	}
	public String getPath() {
		return getPropertyValue(SystemModel.PATH);
	}
	public void setPath(String path) {
		try {
			addProperty(SystemModel.PATH, path);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+SystemModel.PATH+" -> "+path);
		}
	}
	public String getQuery() {
		return getPropertyValue(SystemModel.QUERY);
	}
	public void setQuery(String query) {
		try {
			addProperty(SystemModel.QUERY, query);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+SystemModel.QUERY+" -> "+query);
		}
	}
	public String getIcon() {
		return getPropertyValue(CrawlingModel.ICON);
	}
	public void setIcon(String icon) {
		try {
			addProperty(CrawlingModel.ICON, icon);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.ICON+" -> "+icon);
		}
	}
	public long getTimestamp() {
		String timestamp = getPropertyValue(CrawlingModel.TIMESTAMP);
		if(timestamp != null && timestamp.length() > 0) return Long.valueOf(timestamp);
		return 0;
	}
	public void setTimestamp(long timestamp) {
		try {
			addProperty(CrawlingModel.TIMESTAMP, timestamp);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.TIMESTAMP+" -> "+timestamp);
		}
	}
	public String getSummary() {
		return getPropertyValue(CrawlingModel.SUMMARY);
	}
	public void setSummary(String summary) {
		try {
			addProperty(CrawlingModel.SUMMARY, summary);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.SUMMARY+" -> "+summary);
		}
	}
	public int getHops() {
		String hops = getPropertyValue(CrawlingModel.HOPS);
		if(hops != null && hops.length() > 0) return Integer.valueOf(hops);
		return 1;
	}
	public void setHops(int hops) {
		try {
			addProperty(CrawlingModel.HOPS, hops);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.HOPS+" -> "+hops);
		}
	}
	public int getStatus() {
		String status = getPropertyValue(CrawlingModel.STATUS);
		if(status != null && status.length() > 0) return Integer.valueOf(status);
		return STATUS_PAUSED;
	}
	public void setStatus(int status) {
		try {
			addProperty(CrawlingModel.STATUS, status);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.STATUS+" -> "+status);
		}
	}
	public String getMessage() {
		return getPropertyValue(CrawlingModel.MESSAGE);
	}
	public void setMessage(String message) {
		try {
			addProperty(CrawlingModel.MESSAGE, message);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.MESSAGE+" -> "+message);
		}
	}	
	public String getSeeded() {
		return getPropertyValue(CrawlingModel.SEEDED);
	}
	public void setSeeded(String seeded) {
		try {
			addProperty(CrawlingModel.SEEDED, seeded);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.SEEDED+" -> "+seeded);
		}
	}
	public String getExtracted() {
		return getPropertyValue(CrawlingModel.EXTRACTED);
	}
	public void setExtracted(String extracted) {
		try {
			addProperty(CrawlingModel.EXTRACTED, extracted);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.EXTRACTED+" -> "+extracted);
		}
	}
	public String getContentType() {
		return getPropertyValue(CrawlingModel.CONTENT_TYPE);
	}
	public void setContentType(String message) {
		try {
			addProperty(CrawlingModel.CONTENT_TYPE, message);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.CONTENT_TYPE+" -> "+message);
		}
	}
	
	public int getErrorCount() {
		String hops = getPropertyValue(CrawlingModel.HOPS);
		if(hops != null && hops.length() > 0) return Integer.valueOf(hops);
		return 0;
	}
	public void setErrorCount(int count) {
		try {
			addProperty(CrawlingModel.ERROR_COUNT, count);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.HOPS+" -> "+count);
		}
	}
	public boolean getLoaded() {
		String loaded = getPropertyValue(CrawlingModel.LOADED);
		if(loaded != null && loaded.length() > 0 && loaded.equals("true")) return true;
		return false;
	}
	public void setLoaded(boolean loaded) {
		try {
			addProperty(CrawlingModel.LOADED, loaded);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.LOADED+" -> "+loaded);
		}
	}
	public boolean getAnonymous() {
		String domain = getPropertyValue(CrawlingModel.ANONYMOUS);
		if(domain != null && domain.length() > 0 && domain.equals("true")) return true;
		return false;
	}
	public void setAnonymous(boolean anonymous) {
		try {
			addProperty(CrawlingModel.ANONYMOUS, anonymous);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.ANONYMOUS+" -> "+anonymous);
		}
	}
	public long getLastCrawl() {
		String lastCrawl = getPropertyValue(CrawlingModel.LAST_CRAWL);
		if(lastCrawl != null && lastCrawl.length() > 0) return Long.valueOf(lastCrawl);
		return 0;
	}
	public void setLastCrawl(long lastCrawl) {
		try {
			addProperty(CrawlingModel.LAST_CRAWL, lastCrawl);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.LAST_CRAWL+" -> "+lastCrawl);
		}
	}
	public int getMaxResults() {
		String count = getPropertyValue(CrawlingModel.MAX_RESULTS);
		if(count != null && count.length() > 0) return Integer.valueOf(count);
		return 0;
	}
	public void setMaxResults(int count) {
		try {
			addProperty(CrawlingModel.MAX_RESULTS, count);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.MAX_RESULTS+" -> "+count);
		}
	}
	public int getNumTries() {
		String count = getPropertyValue(CrawlingModel.NUM_TRIES);
		if(count != null && count.length() > 0) return Integer.valueOf(count);
		return 0;
	}
	public void setNumTries(int count) {
		try {
			addProperty(CrawlingModel.NUM_TRIES, count);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.NUM_TRIES+" -> "+count);
		}
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Seed) {
			Seed seed = (Seed)obj;
			return getUrl().equals(seed.getUrl());
		}
		return false;
	}
	
	public String toJson() {
		StringWriter writer = new StringWriter();
		JsonWriter jsonWriter = Json.createWriter(writer);				
		JsonObject obj = toJsonObject();		
		jsonWriter.write(obj);
		return writer.toString();
	}
	public JsonObject toJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();		
		if(getId() != null) builder.add("id", getId());
		if(getUid() != null) builder.add("uid", getUid());
		if(getName() != null) builder.add("name", getName());
		if(getUrl() != null) builder.add("url", getUrl());
		if(getContent() != null && getContent().length > 0) builder.add("content", new String(getContent()));
		builder.add("status", getStatus());
		return builder.build();
	}
}
