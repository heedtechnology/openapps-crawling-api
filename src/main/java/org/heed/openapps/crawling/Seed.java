package org.heed.openapps.crawling;
import java.io.Serializable;
import java.net.URL;

import org.heed.openapps.entity.Entity;


public interface Seed extends Entity, Crawlable, Serializable {
	public static final int STATUS_BLACKLISTED = 5;
	public static final int STATUS_EXTRACTED = 4;
	public static final int STATUS_LOADED = 3;
	public static final int STATUS_HIT = 2;	
	public static final int STATUS_RUNNING = 1;
	public static final int STATUS_PAUSED = 0;
	public static final int STATUS_ERROR = -1;
	
	public void setContent(byte[] content);
	public byte[] getContent();
	
	public void setUrl(URL url);
	public void setUrl(String url);
	public String getUrl();
	public String getProtocol();
	public String getDomain();
	public void setDomain(String domain);
	public String getPath();
	public void setPath(String path);
	public String getQuery();
	public void setQuery(String query);
	public String getIcon();
	public void setIcon(String icon);
	public long getTimestamp();
	public void setTimestamp(long timestamp);
	public String getSummary();
	public void setSummary(String summary);
	public int getHops();
	public void setHops(int hops);
	public int getStatus();
	public void setStatus(int status);
	public String getMessage();
	public void setMessage(String message);
	public String getSeeded();
	public void setSeeded(String seeded);
	public String getExtracted();
	public void setExtracted(String extracted);
	public String getContentType();
	public void setContentType(String message);
	public int getErrorCount();
	public void setErrorCount(int count);
	public boolean getLoaded();
	public void setLoaded(boolean loaded);
	public boolean getAnonymous();
	public void setAnonymous(boolean anonymous);
	public long getLastCrawl();
	public void setLastCrawl(long lastCrawl);
	public int getMaxResults();
	public void setMaxResults(int count);
	public int getNumTries();
	public void setNumTries(int count);
	
}
