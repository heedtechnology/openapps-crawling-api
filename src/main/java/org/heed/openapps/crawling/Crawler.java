package org.heed.openapps.crawling;

import java.io.Serializable;
import java.util.List;

import javax.json.JsonObject;

import org.heed.openapps.entity.Entity;


public interface Crawler extends Entity, Serializable {
	
	public static final int TYPE_GATHERER = 1;
	public static final int TYPE_WATCHER = 2;
	
	public static final int STATUS_RUNNING = 2;
	public static final int STATUS_ACTIVE = 1;
	public static final int STATUS_PAUSED = 0;
	public static final int STATUS_ERROR = -1;
	
	public static final int FREQ_MONTHLY = 1;
	public static final int FREQ_WEEKLY = 2;
	public static final int FREQ_DAILY = 3;
	public static final int FREQ_2DAILY = 4;
	public static final int FREQ_8HOUR = 5;
	public static final int FREQ_4HOUR = 6;
	public static final int FREQ_2HOUR = 7;
	public static final int FREQ_HOUR = 8;
	
	
	public boolean isActive();
	public boolean isRunning();
	public boolean isPaused();
	
	//public void setEntity(Entity entity);
	public void setUrl(String urlStr);
	public String getUrl();
	
	public Long getId();
	public String getName();
	public int getType();
	public void setType(int type);
	public String getUsername();
	public String getPassword();
	public String getProtocol();
	public String getDomain();
	public String getPath();
	public String getQuery();
	public String getCategory();
	public void setCategory(String category);
	public long getIcon();
	public void setIcon(long id);
	public long getImage();
	public void setImage(long id);
	public String getMessage();
	public void setMessage(String message);
	public int getStatus();
	public void setName(String name);
	public void setStatus(int status);
	public int getFrequency();
	public void setFrequency(int frequency);
	public long getLastCrawl();
	public void setLastCrawl(long lastCrawl);
	public String getDayOfWeek();
	public void setDayOfWeek(String dayOfWeek);
	public String getTimeOfDay1();
	public void setTimeOfDay1(String timeOfDay);
	public String getTimeOfDay2();
	public void setTimeOfDay2(String timeOfDay);
	public int getHops();
	public void setHops(int hops);
	public int getMaxResults();
	public void setMaxResults(int count);
	public boolean isSameDomain();
	public void setSameDomain(boolean domain);
	public boolean isLoaded();
	public void setLoaded(boolean loaded);
	public boolean isAnonymous();
	public void setCached(boolean cached);
	public boolean isCached();
	public void setAnonymous(boolean anonymous);
	public long getNextCrawl();
	public void setNextCrawl(long nextCrawl);
	public String getConfiguration();
	public void setConfiguration(String configuration);
	public String getConfigurationId();
	public void setConfigurationId(String configuration);
	
	public byte[] getIconData();
	public void setIconData(byte[] iconData);
	public int getNewDocuments();
	public void setNewDocuments(int newDocuments);
	public String getLastMessage();
	public void setLastMessage(String lastMessage);
	public List<Search> getSearches();
	public void setSearches(List<Search> searches);
	public List<Seed> getSeeds();
	public void setSeeds(List<Seed> seeds);
	
	public String toJson();
	JsonObject toJsonObject();
}
