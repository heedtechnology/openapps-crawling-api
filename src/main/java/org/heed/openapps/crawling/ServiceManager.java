package org.heed.openapps.crawling;

import org.heed.openapps.content.ContentService;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.property.PropertyService;


public interface ServiceManager {

	PropertyService getPropertyService();
	DataDictionaryService getDictionaryService();
	EntityService getEntityService();
	CrawlingService getCrawlingService();
	ContentService getContentService();
	
}
