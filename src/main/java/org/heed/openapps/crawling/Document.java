package org.heed.openapps.crawling;
import java.io.Serializable;

import javax.json.JsonObject;

import org.heed.openapps.entity.Entity;


public interface Document extends Entity, Serializable {
	public static final int STATUS_PUBLIC = 3;
	public static final int STATUS_ARCHIVED = 2;
	public static final int STATUS_READ = 1;
	public static final int STATUS_DELETED = -1;
	
	public void setEntity(Entity entity);
	public void setUrl(String url);
	public String getUrl();
	
	public String getProtocol();
	public void setProtocol(String protocol);
	public String getDomain();
	public void setDomain(String domain);
	public String getPath();
	public void setPath(String path);
	public String getFile();
	public void setFile(String file);
	public String getQuery();
	public void setQuery(String query);
	public String getImageUrl();
	
	public void setImageUrl(String url);
	public long getImage();
	public void setImage(long id);
	public long getTimestamp();
	public void setTimestamp(long timestamp);
	public String getDisplayDate();
	public void setDisplayDate(String date);
	public String getMessage();
	public void setMessage(String message);
	public String getSummary();
	public void setSummary(String summary);
	public String getContent();
	public void setContent(String message);
	public String getJournal();
	public void setJournal(String journal);
	public String getContentType();
	public void setContentType(String message);
	public boolean getLoaded();
	public void setLoaded(boolean loaded);
	public boolean getViewed();
	public void setViewed(boolean viewed);
	public boolean getFlagged();
	public void setFlagged(boolean flagged);
	public int getStatus();
	public void setStatus(int status);
	
	public String getDate();
	public void setDate(String date);
	public String getTimezone();
	public void setTimezone(String timezone);
	public byte[] getImageData();
	public void setImageData(byte[] imageData);
	public byte[] getDocumentData();
	public void setDocumentData(byte[] documentData);
	public Long getCrawlerId();
	public void setCrawlerId(Long crawlerId);
	
	public String toJson();
	JsonObject toJsonObject();
}
