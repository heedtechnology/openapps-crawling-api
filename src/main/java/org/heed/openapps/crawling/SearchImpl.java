package org.heed.openapps.crawling;

import javax.json.JsonObject;

import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;

public class SearchImpl extends EntityImpl implements Search {
	private static final long serialVersionUID = 4020915406392831327L;
	
	
	public SearchImpl() {
		setQName(CrawlingModel.SEARCH);
	}
	public SearchImpl(String query) {
		setQName(CrawlingModel.SEARCH);
		setName(query);
	}
	
	public SearchImpl(Entity entity) {
		setNode(entity.getNode());
		setProperties(entity.getProperties());
		setSourceAssociations(entity.getSourceAssociations());
		setTargetAssociations(entity.getTargetAssociations());
	}
	
	public void setEntity(Entity entity) {
		setNode(entity.getNode());
		setProperties(entity.getProperties());
		setSourceAssociations(entity.getSourceAssociations());
		setTargetAssociations(entity.getTargetAssociations());
	}
	public SearchImpl(JsonObject object) {
		setQName(CrawlingModel.SEARCH);
		if(object.containsKey("id")) setId(object.getJsonNumber("id").longValue());
		if(object.containsKey("uid")) setUid(object.getString("uid"));
		if(object.containsKey("name")) setName(object.getString("name"));
		if(object.containsKey("active")) setActive(object.getBoolean("active"));
	}
	
	public boolean isActive() {
		String loaded = getPropertyValue(CrawlingModel.ACTIVE);
		if(loaded != null && loaded.length() > 0 && loaded.equals("true")) return true;
		return false;
	}
	public void setActive(boolean loaded) {
		try {
			addProperty(CrawlingModel.LOADED, loaded);
		} catch(Exception e) {
			System.out.println("InvalidPropertyException "+CrawlingModel.ACTIVE+" -> "+loaded);
		}
	}
}
